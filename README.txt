Chen:
    Alpha-Beta Pruning
    Experimented with vulnerability (not used)
    Iterative deepening (not used)

Sith:
    Initial evaluation + Heuristics
    Experimented with Heuristics
    Depth First Search

Tournament:
    Heuristics is the most important part.  Tried experimenting with 
    mobility but didn't work too well since it played very little part.
    Doing Depth First Search is very useful.  However, each stage require
    a different optimized heuristics.  We also tried Itererative Deepening
    but couldn't fine the right heuritics.
