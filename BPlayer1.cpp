#include "player.h"
#include <ctime>
#define DEPTH 4
#define MAX_DEPTH 6
#define POS_INF 1073741823
#define NEG_INF -1073741823
  
static int heuristic1[64] = 
   {  8, -6,  4,  4,  4,  4, -6,  8,
     -6, -8,  4,  2,  2,  4, -7, -6,
      4,  4,  2,  1,  1,  2,  4,  4,
      4,  2,  1,  1,  1,  1,  2,  4,
      4,  2,  1,  1,  1,  1,  2,  4,
      4,  4,  2,  1,  1,  2,  4,  4,
     -6, -7,  4,  2,  2,  4, -7, -6,
      8, -6,  4,  4,  4,  4, -6,  8
    }; 
    
static int def_heuristic[64] = 
   { 16, -9, 5, 4, 4, 5, -9, 16,
     -9,-15, 4, 2, 2, 4,-15, -9,
      5,  4, 4, 1, 1, 4,  4,  5,
      4,  2, 1, 1, 1, 1,  2,  4,
      4,  2, 1, 1, 1, 1,  2,  4,
      5,  4, 4, 1, 1, 4,  4,  5,
     -9,-15, 4, 2, 2, 4,-15, -9,
     16, -9, 5, 4, 4, 5, -9, 16
    };
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    me = side;
     
    if (me == WHITE) {
        opponent = BLACK;
    }
    else {
        opponent = WHITE;
    }
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
  return minimaxMove(opponentsMove, msLeft);
}

/* Computes the next move via mini-max. Goes into one level of depth to track the move,
 * calls helper function minimax_recur to compute value of children moves. Depth is
 * specified in the macro currently at the head of player.cpp (this file). Depth should
 * be an odd number, and the algorithm will start to drastically slow at depth 5.
 */
Move *Player::minimaxMove(Move *opponentsMove, int msLeft) {
  //cerr << msLeft << endl;
  clock_t start = clock();
  int movesLeft = (64 - board.countWhite() - board.countBlack());
  //cerr << movesLeft << endl;
    if (opponentsMove != NULL) {
        board.doMove(opponentsMove, opponent);
    }  
    if (!board.hasMoves(me)) {
        return NULL;
    }

    int bestscore = NEG_INF;
    int bestindex = 0;

   
    vector<Move> possible = board.posMoves(me);
    for (unsigned int i = 0; i < possible.size(); i++) {
      Board * bcopy = board.copy();
      bcopy->doMove(&possible[i], me);
      //Look to depth 4, runs very quickly
      int score = minimax_recur(bcopy, &possible[i], opponent, NEG_INF, POS_INF, DEPTH-1);
      if (score > bestscore) {
	bestscore = score;
	bestindex = i;
      }
    }
    
    //Iterative Deepening
    clock_t timer = clock() - start;
    float t = 1000*(float)timer/CLOCKS_PER_SEC;
    int depth = DEPTH;
    while (depth < MAX_DEPTH && (msLeft < 0 || t < 2*(msLeft/movesLeft) - 1000)) {
      for (unsigned int i = 0; i < possible.size(); i++) {
        Board * bcopy = board.copy();
        bcopy->doMove(&possible[i], me);
        int score = minimax_recur(bcopy, &possible[i], opponent, NEG_INF, POS_INF, depth);
        if (score > bestscore) {
  	  bestscore = score;
	  bestindex = i;
        }
      }
      depth++;
      timer = clock() - start;
      cerr << depth << endl;
    }
    

    board.doMove(&possible[bestindex], me);
    
    return new Move(possible[bestindex].getX(), possible[bestindex].getY());
}

/* Recursively calculates mini-max value of a move, maximizing at our move
 * and minimizing at an opponent move. 
 *
 * Algorithm uses alpha-beta pruning and only goes to a specified depth.
 */
int Player::minimax_recur(Board *board, Move *opponentsMove, Side side, int alpha, int beta, int depth) {
    if (opponentsMove != NULL) {
        board->doMove(opponentsMove, side == me ? opponent : me);
    }  
    if (depth == 0) {
      return playerEval(board);
    }

    if (side == me) {
       vector<Move> possible = board->posMoves(me);
       for (unsigned int i = 0; i < possible.size(); i++) {
	 Board *bcopy = board->copy();
	 int score = minimax_recur(bcopy, &possible[i], opponent, alpha, beta, depth - 1);
	 if (score > alpha) {
	   alpha = score;
	 }
	 if (alpha >= beta) {
	   return alpha;
	 }
       }
       return alpha;
    }
    else {
       vector<Move> possible = board->posMoves(opponent);
       for (unsigned int i = 0; i < possible.size(); i++) {
	 Board *bcopy = board->copy();
	 int score = minimax_recur(bcopy, &possible[i], me, alpha, beta, depth - 1);
	 if (score < beta) {
	   beta = score;
	 }
	 if (beta <= alpha) {
	   return beta;
	 }
       }
       return beta;
    }
}

int Player::playerEval(Board *board) {
  int base = board->eval(def_heuristic, me);
  base += board->count(me);
  base -= board->count(opponent);
  return base;
}
