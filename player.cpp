#include "player.h"
#include <ctime>
#define DEPTH 4
#define MAX_DEPTH 4
#define POS_INF 1073741823
#define NEG_INF -1073741823

// Lame heuristic
static int diff_heuristic[64] = 
    { 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1
    }; 
    
/*
 * Heuristics for different states of the board
 * 
 * 1. Standard Case
 * 2. Corners taken
 */     
static int def_heuristic[64] = 
   { 16, -9, 5, 4, 4, 5, -9, 16,
     -9,-15, 4, 1, 1, 4,-15, -9,
      5,  4, 4, 0, 0, 4,  4,  5,
      4,  1, 0, 0, 0, 0,  1,  4,
      4,  1, 0, 0, 0, 0,  1,  4,
      5,  4, 4, 0, 0, 4,  4,  5,
     -9,-15, 4, 1, 1, 4,-15, -9,
     16, -9, 5, 4, 4, 5, -9, 16
    };
    
static int cornerA[64] = 
    {15, 0, 6, 5, 4, 8, 0,15,
      0, 6, 6, 5, 1, 2,-8, 0,
      6, 6, 5, 5, 2, 2, 2, 4,
      5, 5, 5, 1, 1, 2, 1, 4,
      4, 1, 2, 1, 1, 2, 1, 4,
      8, 2, 2, 2, 2, 2, 2, 4,
      0,-8, 2, 1, 1, 2,-8, 0,
     15, 0, 4, 4, 4, 4, 0,15
    }; 

static int cornerB[64] = 
    {15, 0, 8, 4, 5, 6, 0,15,
      0,-8, 2, 1, 5, 6, 6, 0,
      4, 2, 2, 2, 5, 5, 6, 6,
      4, 1, 2, 1, 1, 5, 5, 5,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 2, 2, 2, 2, 2, 2, 8,
      0,-8, 2, 1, 1, 2, 0, 0,
     15, 0, 4, 4, 4, 4, 0,15
    }; 

static int cornerC[64] = 
    {15, 0, 4, 4, 4, 4, 0,15,
      0,-8, 2, 1, 1, 2,-8, 0,
      8, 2, 2, 2, 2, 2, 2, 4,
      4, 1, 2, 1, 1, 2, 1, 4,
      5, 5, 5, 1, 1, 2, 1, 4,
      6, 6, 5, 5, 2, 2, 2, 4,
      0, 6, 6, 5, 1, 2,-8, 0,
     15, 0, 6, 5, 4, 8, 0,15
    };
    
static int cornerD[64] = 
    {15, 0, 4, 4, 4, 4, 0,15,
      0,-3, 2, 1, 1, 2,-8, 0,
      4, 2, 2, 2, 2, 2, 2, 8,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 1, 2, 1, 1, 5, 5, 5,
      4, 2, 2, 2, 5, 5, 6, 6,
      0, 0, 2, 1, 5, 6, 6, 0,
     15, 0, 8, 4, 5, 6, 0, 15
    }; 

static int cornerAB[64] = 
    { 8, 7, 5, 4, 4, 5, 7, 8,
      7, 5, 5, 1, 1, 5, 5, 7,
      6, 5, 2, 2, 2, 2, 5, 6,
      5, 5, 2, 1, 1, 2, 5, 5,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 2, 2, 2, 2, 2, 2, 4,
      0,-3, 2, 1, 1, 2,-3, 0,
      9, 0, 4, 4, 4, 4, 0, 9
    }; 

static int cornerAC[64] = 
    { 8, 7, 5, 4, 4, 4, 0, 9,
      7, 5, 5, 1, 1, 2,-3, 0,
      5, 5, 2, 2, 2, 2, 2, 4,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 1, 2, 1, 1, 2, 1, 4,
      5, 5, 2, 2, 2, 2, 2, 4,
      7, 5, 5, 1, 1, 2,-3, 0,
      8, 7, 5, 4, 4, 4, 0, 9
    }; 

static int cornerBD[64] = 
    { 9, 0, 4, 4, 4, 5, 7, 8,
      0,-3, 2, 1, 1, 5, 5, 7,
      4, 2, 2, 2, 2, 2, 5, 5,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 2, 2, 2, 2, 2, 5, 5,
      0,-3, 2, 1, 1, 5, 5, 7,
      9, 0, 4, 4, 4, 5, 7, 8
    };

static int cornerCD[64] = 
    { 9, 0, 4, 4, 4, 4, 0, 9,
      0,-3, 2, 1, 1, 2,-3, 0,
      4, 2, 2, 2, 2, 2, 2, 4,
      4, 1, 2, 1, 1, 2, 1, 4,
      4, 1, 2, 1, 1, 2, 1, 1,
      5, 5, 2, 2, 2, 2, 5, 5,
      7, 5, 5, 5, 5, 5, 5, 7,
      8, 7, 5, 4, 4, 5, 7, 8
    };      
    
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    me = side;
     
    if (me == WHITE) {
        opponent = BLACK;
    }
    else {
        opponent = WHITE;
    }
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
  //return randomMove(opponentsMove, msLeft);
  //return greedyMove(opponentsMove, msLeft);
  //return bfsMove(opponentsMove, msLeft);
  return minimaxMove(opponentsMove, msLeft);
}

Move *Player::randomMove(Move *opponentsMove, int msLeft) {
    int random;
    
    if (opponentsMove != NULL) {
        board.doMove(opponentsMove, opponent);
    }
         
    if (!board.hasMoves(me)) {
        return NULL;
    }
    
    vector<Move> possible = board.posMoves(me);
    
    // Init Random
    srand (time(NULL));
    random = rand() % (possible.size());
    
    int x = possible[random].getX();
    int y = possible[random].getY();
    
    Move * play = new Move(x, y); 
    
    board.doMove(play, me);
    
    return play;
}

Move *Player::greedyMove(Move *opponentsMove, int msLeft){
    if (opponentsMove != NULL) {
        board.doMove(opponentsMove, opponent);
    }
         
    if (!board.hasMoves(me)) {
        return NULL;
    }
    
    vector<Move> possible = board.posMoves(me);

    int best_move_ind = 0;
    Board * bcopy = board.copy();
    bcopy->doMove(&possible[0], me);
    int best_score = bcopy->eval(def_heuristic, me);

    for (unsigned int i = 1; i < possible.size(); i++) {
        bcopy = board.copy();
        bcopy->doMove(&possible[i], me);
        
        int score = bcopy->eval(def_heuristic, me);
        if (score > best_score) {
            best_score = score;
            best_move_ind = i;
        }
    }
   
    board.doMove(&possible[best_move_ind], me);
    
    return new Move(possible[best_move_ind].getX(), possible[best_move_ind].getY());
}

/*
 * Board evaluation with the correct heuristics
 * 
 */ 
int Player::playerEval(Board *scopy) {
    int score;
    
    if (testingMinimax) {
        score = scopy->eval(diff_heuristic, me);
    }
    
    else if (scopy->get(me, 0, 0) && scopy->get(me, 0, 7)) {
        score = scopy->eval(cornerAB, me);
    }
    else if (scopy->get(me, 0, 7) && scopy->get(me, 7, 7)) {
        score = scopy->eval(cornerCD, me);
    }
    else if (scopy->get(me, 0, 0) && scopy->get(me, 0, 7)) {
        score = scopy->eval(cornerAC, me);
    }
    else if (scopy->get(me, 7, 0) && scopy->get(me, 7, 7)) {
        score = scopy->eval(cornerBD, me);
    }    
    
    else if (scopy->get(me, 0, 7)) {
        score = scopy->eval(cornerC, me);
    }
    else if (scopy->get(me, 7, 7)) {
        score = scopy->eval(cornerD, me);
    }
    else if (scopy->get(me, 0, 0)) {
        score = scopy->eval(cornerA, me);
    }
    else if (scopy->get(me, 7, 0)) {
        score = scopy->eval(cornerB, me);
    }
    
    else {
        score = scopy->eval(def_heuristic, me);
    }
    
    return score;
}

Move *Player::bfsMove(Move *opponentsMove, int msLeft) {
    
    if (opponentsMove != NULL) {
        board.doMove(opponentsMove, opponent);
    }
         
    if (!board.hasMoves(me)) {
        return NULL;
    }
    
    int score;
    int bestscore = -9000;
    int index = 0;
    vector<Move> possible = board.posMoves(me);
    
    for (unsigned int i = 0; i < possible.size(); i++) {
        int min = 100;
        
        Board * bcopy = board.copy();
        bcopy->doMove(&possible[i], me);
        
        vector<Move> check = bcopy->posMoves(opponent);
        
        for (unsigned int j = 0; j < check.size(); j++) {
            Board * scopy = bcopy->copy();
            scopy->doMove(&check[j], opponent);
            
            score = playerEval(scopy);
	    //score = scopy->eval(def_heuristic, me);
            
            if (score < min) {
                min = score;
            }
        }

        if (min > bestscore) {
            bestscore = min;
            index = i;
        }
    }
   
    board.doMove(&possible[index], me);
    
    return new Move(possible[index].getX(), possible[index].getY());
}

/* Computes the next move via mini-max. Goes into one level of depth to track the move,
 * calls helper function minimax_recur to compute value of children moves. Depth is
 * specified in the macro currently at the head of player.cpp (this file). Depth should
 * be an odd number, and the algorithm will start to drastically slow at depth 5.
 */
Move *Player::minimaxMove(Move *opponentsMove, int msLeft) {
  //cerr << msLeft << endl;
  clock_t start = clock();
  int movesLeft = (64 - board.countWhite() - board.countBlack());
  //cerr << movesLeft << endl;
    if (opponentsMove != NULL) {
        board.doMove(opponentsMove, opponent);
    }  
    if (!board.hasMoves(me)) {
        return NULL;
    }

    int bestscore = NEG_INF;
    int bestindex = 0;

   
    vector<Move> possible = board.posMoves(me);
    for (unsigned int i = 0; i < possible.size(); i++) {
      Board * bcopy = board.copy();
      bcopy->doMove(&possible[i], me);
      //Look to depth 4, runs very quickly
      int score = minimax_recur(bcopy, &possible[i], opponent, NEG_INF, POS_INF, DEPTH-1);
      if (score > bestscore) {
	bestscore = score;
	bestindex = i;
      }
    }
    
    //Iterative Deepening
    clock_t timer = clock() - start;
    float t = 1000*(float)timer/CLOCKS_PER_SEC;
    int depth = DEPTH;
    while (depth < MAX_DEPTH && (msLeft < 0 || t < 2*(msLeft/movesLeft) - 1000)) {
      for (unsigned int i = 0; i < possible.size(); i++) {
        Board * bcopy = board.copy();
        bcopy->doMove(&possible[i], me);
        int score = minimax_recur(bcopy, &possible[i], opponent, NEG_INF, POS_INF, depth);
        if (score > bestscore) {
  	  bestscore = score;
	  bestindex = i;
        }
      }
      depth++;
      timer = clock() - start;
      cerr << depth << endl;
    }
    

    board.doMove(&possible[bestindex], me);
    
    return new Move(possible[bestindex].getX(), possible[bestindex].getY());
}

/* Recursively calculates mini-max value of a move, maximizing at our move
 * and minimizing at an opponent move. 
 *
 * Algorithm uses alpha-beta pruning and only goes to a specified depth.
 */
int Player::minimax_recur(Board *board, Move *opponentsMove, Side side, int alpha, int beta, int depth) {
    if (opponentsMove != NULL) {
        board->doMove(opponentsMove, side == me ? opponent : me);
    }  
    if (depth == 0) {
      return board->eval(def_heuristic, me);
    }

    if (side == me) {
       vector<Move> possible = board->posMoves(me);
       for (unsigned int i = 0; i < possible.size(); i++) {
	 Board *bcopy = board->copy();
	 int score = minimax_recur(bcopy, &possible[i], opponent, alpha, beta, depth - 1);
	 if (score > alpha) {
	   alpha = score;
	 }
	 if (alpha >= beta) {
	   return alpha;
	 }
       }
       return alpha;
    }
    else {
       vector<Move> possible = board->posMoves(opponent);
       for (unsigned int i = 0; i < possible.size(); i++) {
	 Board *bcopy = board->copy();
	 int score = minimax_recur(bcopy, &possible[i], me, alpha, beta, depth - 1);
	 if (score < beta) {
	   beta = score;
	 }
	 if (beta <= alpha) {
	   return beta;
	 }
       }
       return beta;
    }
}

