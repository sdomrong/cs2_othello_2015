#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <vector>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    // bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    vector<Move> posMoves(Side side);     // Returns a list of all possible moves
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int eval(int heuristic[], Side side); // Returns the value of a board based on a given heuristic
    int vulEval(int heuristic[], Side side);
    void setBoard(char data[]);
    bool get(Side side, int x, int y);
    bool on(int x, int y);
};

#endif
