#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

class Player {

private:
    Board board;
    Side me;
    Side opponent;
    
    int playerEval(Board *scopy);
    int minimax_recur(Board *board, Move *opponentsMove, Side side, int alpha, int beta, int depth);

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *randomMove(Move *opponentsMove, int msLeft);
    Move *greedyMove(Move *opponentsMove, int msLeft);
    Move *bfsMove(Move *opponentsMove, int msLeft);
    Move *minimaxMove(Move *opponentsMove, int msLeft);

    void setBoard(Board * b) { board = *b; }

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
